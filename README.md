# Elation signature composer

This is a signature composer built for Elation. This project was bootstrapped
with [Create React App](https://github.com/facebookincubator/create-react-app).
An online version is available [here](https://elation-signature-composer.netlify.com/).

## Requisites

- Node.js v5 or superior

## Getting started

First, in order to clone the project you can run:

```shell
$ git clone http://repo.or.cz/elation-signature-composer.git
```

Then enter into the project directory and install all dependencies.

```shell
$ cd elation-signature-composer
$ npm install
```

That's it! now you can start playing with any of the available scripts

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.

## Deployment

I'm using [Netlify](https://www.netlify.com/) to deploy the app, however the deployment
just consists of uploading the files under `build/` to a server.
